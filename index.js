const express = require("express")
const app = express()
const path = require("path")

const laptops = []

app.use(express.static(path.join(__dirname, "public")))
app.use(express.json())

app.get("/", (req, res) => {
	return res.sendFile(path.join(__dirname, "public", "index.html"))
})

// FETCHING FROM SERVER API
app.get("/api/computers", (req, res) => {
	return res.status(200).json(laptops)
})

app.listen(3000, () => {
	console.log("listening on port 3000....")
})
